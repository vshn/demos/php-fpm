# PHP FPM Demo Project

Demo Project to demonstrate an Nginx/php-fpm setup.

## Development

To test `kaniko` builds locally, run

    docker run -it --rm -v "${PWD}:/workspace" gcr.io/kaniko-project/executor:debug --dockerfile ./docker/nginx/Dockerfile --context dir:///workspace/ --no-push


## Deployment

    kubectl apply -k ./deploy
